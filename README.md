#Projet de labyrinthe

Réalisé par Stéphane Wilhelm et Laura Verin durant la formation Master 1 Géomatique à l'ENSG

Réutilisé dans le cadre du TP d'Intégration Continue avec Damien Duportal

## Prérequis

- Python 3
- numpy
- matplotlib
- tkinter

## Installation

Se mettre dans un répertoire et lancer (sur Linux):
```
git clone https://gitlab.com/StepWilh/projet_labyrinthe_python.git
```

## Démarage

Lancer le programme "__init__.py" et et suivre les instructions affichées dans le terminal.

Le programme fonctionne à partir du moment où la dernière instruction affichée est:
">>> voulez vous sortir du programme? (o:oui/n:non)".

## Utilisation

Lorsque "(o:oui/n:non)" est affiché, rentrer directement la valeur demantée ("o" pour oui et "n" pour non).

Lorsque la fenêtre contenant le labyrinthe s'affiche (et une fois le labyrinthe généré résolu), fermer pour passer à l'étape suivante, c'est-à-dire l'affichage de la solution.