import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="projet-labyrinthe-STEPHANE-WILHELM",
    version="0.0.1",
    author="Stéphane Wilhelm, Laura Verin",
    author_email="stephane.wilhelm@ensg.eu",
    description="projet de generation de labyrinthe et chemin reliant l'entree et la sortie",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/StepWilh/projet_labyrinthe_python.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: Windows, Linux",
    ],
    python_requires='>=3.6',

)
