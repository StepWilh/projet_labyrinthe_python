#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Stephane Wilhelm - Laura Verin
@date  : 11/2018
"""
#import sys
#sys.path.append("src/main")
#from src.main.labyrinthe_generation import *

from labyrinthe_generation import *

#nous voulons afficher un chemin entre l'entrée et la sortie d'un labyrinthe généré aléatoirement
"""
pos : position actuelle du point qui parcourt le labyrinthe en renvoyant les coordonnée de la matrice ou la position est située 
(exemple: [1,1] pour la case de départ en haut à gauche)
L : labyrinthe ou on souhaite créer un chemin entre l'entrée et la sortie
inrtersect : liste des intersections de chemin rencontrés dans le labyrinthe par la position
"""

#pour générer le chemin du labyrinthe, nous utilisons 3 listes  constituées de coordonnées de la matrice du labyrinthe:
#une liste "chemin" qui contiendra le tracé du chemin à afficher entre l'entrée et la sortie du labyrinthe
#une liste "cases_interdites" qui contiendra les cases où la position de devra plus retourner pour créer le chemin
#une liste "intersection" qui contiendra les cases correspondantà une intersection entre plusieurs chemins possibles où la position peut aller

#nous avons une méthode qui teste si la position se trouve dans une intersection de labyrinthe




# on déplace la position en incrémentant une coordonnée de la position selon la direction à prendre (Nord, Sud, Est, Ouest)
# la methode prend en entrée les coordonnées de la position "pos" les voisins de "pos" la liste des cases interdites et les intersections
def deplacement_position(pos,voisins_pos,interdit,intersect):

    # les cases voisines sont définies de la façon suivantes : [Nord, Sud, Est, Ouest]
    # 0 = case libre "vide"
    # 1 = mur
    # on fait avancer la position selon si la case est égale à 0 "vide" et si elle n'est pas déja passée par cette case avec la liste "interdit" et "intersect"
    if voisins_pos[0] == 0 and [pos[0]-1,pos[1]] not in interdit and [pos[0]-1,pos[1]] not in intersect:
        pos = [pos[0]-1,pos[1]]
    elif voisins_pos[1] == 0 and [pos[0]+1,pos[1]] not in interdit and [pos[0]+1,pos[1]] not in intersect:
        pos = [pos[0]+1,pos[1]]
    elif voisins_pos[3] == 0 and [pos[0],pos[1]-1] not in interdit and [pos[0],pos[1]-1] not in intersect:
        pos = [pos[0],pos[1]-1]
    elif voisins_pos[2] == 0 and [pos[0],pos[1]+1] not in interdit and [pos[0],pos[1]+1] not in intersect:
        pos = [pos[0],pos[1]+1]
    return pos
    
#nous faisons le test si la position a rencontré une cul-de-sac, c'est à dire, si la table des cases voisines de la position est composé de 3 murs
#si la position rencontre un cul de sac, elle revient à la dernière intersection rencontrée (la dernière ajoutée dans la liste)
#on ne doit plus revenir dans la direction de l'intersection qui mène à un cul-de-sac.
def cul_de_sac(pos,voisins_pos,intersect):
    nb1 = 0
    for i in voisins_pos:
        if i == 1:
            nb1 += 1
    if  intersect != [] and nb1 >= 3 :
        pos = intersect[len(intersect)-1]
    return pos

#dans cette méthode, nous faisons le test si, depuis la dernière intersection, tous les chemins mènent à des culs-de-sac.
#si c'est le cas, la position "pos" doit revenir à l'embranchement de chemins précédent pour trouver d'autres chemins possibles jusqu'à l'arrivée
def chemin_faux(L,pos,intersect, interdit):
    #on recherche les valeurs 0 ou 1 autour de la position
    vn = L[pos[0]-1][pos[1]]
    vs = L[pos[0]+1][pos[1]]
    ve = L[pos[0]][pos[1]+1]
    vw = L[pos[0]][pos[1]-1]
       
    #on recherche les coordonnées des cases voisines de la position
    pn = [pos[0]-1,pos[1]]
    ps = [pos[0]+1,pos[1]]
    pe = [pos[0],pos[1]+1]
    pw = [pos[0],pos[1]-1]
    
    if pos in intersect:
        if (vn == 1 or pn in interdit) and (vs == 1 or ps in interdit) and (ve == 1 or pe in interdit) and (vw == 1 or pw in interdit):
            intersect.remove(intersect[len(intersect)-1])
            pos = intersect[len(intersect)-1]
    return pos

#nous devons nous occuper de notre liste chemin qui contient les coordonnées ou notre position "pos" est passé.
#les cases qui sont dans un chemin qui mène à un cul-de-sac sont supprimés pour avoir notre tracé du chemin directement du point de départ et d'arrivée
#un chemin qui contient deux fois la même coordonnée dans la liste signifie une intersection où la position est passée deux fois. C'est à dire qu'il y avait un cul de sac entre les deux même coordonnées
#nous devons supprimer les coordonnées dans la liste entre deux valeurs identiques.
def chemin_final(chemin):
    for i in range (len(chemin)) :
        for j in range (i+1,len(chemin)-1):
            if chemin[i] == chemin[j]:
                chemin = chemin[:i] + chemin[j:]    
    return chemin

#nous utilisons les méthodes créées précedement pour générer notre chemin dans le labyrinthe.
def chemin(L,h,l):
    
    """
    depart : coordonnées du point de départ dans la matrice
    arrive : coordonnées du point d'arrivée dans la matrice
    """
    
    depart = [1,1]
    arrive = [h*2-1,l*2-1]
    
    #initialisation des tables chemin interdits, intersections renconntrées
    chemin = []
    pos = depart
    interdit = []
    intersect = []
    
    while pos != arrive:
        #on enregistre les valeurs des cases voisines : voisins = [n,s,e,w]
        voisins_pos = [L[pos[0]-1][pos[1]],L[pos[0]+1][pos[1]],L[pos[0]][pos[1]+1],L[pos[0]][pos[1]-1]]    
        #test pos sur intersection
        nb0 = 0
        for i in voisins_pos:
            if i == 0:
                nb0 += 1
        if (nb0 >= 3) and (pos not in intersect):
            intersect += [[pos[0],pos[1]]]
        else:    
            interdit += [[pos[0],pos[1]]]    
        chemin += [[pos[0],pos[1]]] 

        #deplacement position
        pos = deplacement_position(pos,voisins_pos,interdit,intersect)
        #cul de sac + retour de la position à l'intersection
        pos = cul_de_sac(pos,voisins_pos,intersect)
        #chemin faux + retour à l'intersection avant la précédente
        pos = chemin_faux(L,pos,intersect, interdit)
        #redefinition du chemin
        chemin = chemin_final(chemin)

    interdit += [pos] 
    chemin += [pos]
    return chemin

# on souhaite maintenant représenter le chemin, le point d'entrée et le point de sortie par des symboles ponctuels
# le chemin est affiché selon la liste de coordonnées créée par le programme en amont
def affichage_labyrinthe_chemin(L,chemin):

    pyplot.figure(figsize=(10, 12))
    pyplot.imshow(L, cmap=pyplot.cm.binary, interpolation='nearest')
    #on affiche un point sur chaque case qui constitue le chemin
    for i in range(len(chemin)):
        pyplot.plot(chemin[i][1], chemin[i][0], 'bs', color='blue', markersize=4)
    pyplot.plot(1, 1, 'bs', color ='green', markersize=8)
    pyplot.plot(len(L[0])-2, len(L)-2, 'bs', color='red', markersize=8)

    pyplot.xticks([]), pyplot.yticks([])
    pyplot.show()


    
    