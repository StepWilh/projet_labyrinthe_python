#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Stephane Wilhelm - Laura Verin
@date  : 11/2018
"""

from chemin_generation import *
import time


#le labyrinthe et le chemin s'affiche via une fenêtre de commande du système

#nous demandons à l'utilisateur la largeur et la hauteur du labyrinthe avant la génération:
#l'utilisateur doit obligatoirement donner des valeurs intégrales et non une valeur à virgule, négative ou un string
def demande_valeur(taille):
    inTaille = input(">>> donnez une {0} pour le labyrinthe :".format(taille))
    try:
        taille = int(inTaille)
    except ValueError:
        print(">>> une valeur numerique est demandee pour la {0}!".format(taille))
    return taille

#le temps de calcul de la génération du labyrinthe est calculé avec la commande "time()"
#on calcule l'écart de temps en secondes entre le début d'exécution du programme et la fin
def execution_generation_labyrinthe_plus_temps(l,h):
    print(">>> generation du labyrinthe...")
    time_start = time.time()
    laby = generation_labyrinthe(l,h)
    print(">>> generation terminee")
    print(">>> parametres : largeur = {0}, hauteur = {1}".format(l,h))
    print(">>> temps d'execution: {0} secondes".format(round(time.time()-time_start,2)))
    print(">>> reliez le point de depart (point vert) et le point d'arrivee (point rouge)")
    print(">>> fermez si termine")
    return laby

#cette méthode détermine le chemin du labyrinthe précédement généré et, comme pour la méthode d'avant, trouve le temps de calcul du programme
def execution_generation_chemin_plus_temps(laby,l,h):
    print(">>> generation d'un chemin...")
    time_start = time.time()
    chemfinal = chemin(laby,l,h)
    print(">>> generation terminee")
    print(">>> temps d'execution: {0} secondes".format(round(time.time()-time_start,2)))
    print(">>> fermez si termine")
    return chemfinal
    
#cette methode contient le "corps" de l'exécution de notre programme dans une fenêtre de commande qui utilise python
def programme_labyrinthe():
    while True:
        largeur = demande_valeur("largeur")
        hauteur = demande_valeur("hauteur")
        #les valeurs de largeur et de hauteur doivent correspondre à des valeurs positives "int" supérieur à 1
        #(un labyrinthe de largeur ou hauteur 1 est constitué d'un unique couloir!)
        if isinstance(largeur,int) == True and isinstance(hauteur,int) == True and largeur > 1 and hauteur > 1 :
            laby = execution_generation_labyrinthe_plus_temps(largeur,hauteur)
            affichage_labyrinthe(laby)
            while True:
                inSolution = input(">>> afficher le chemin? (o:oui/n:non) :")
                if inSolution == "o":
                    chemfinal = execution_generation_chemin_plus_temps(laby,hauteur,largeur)
                    affichage_labyrinthe_chemin(laby,chemfinal)
                    break
                elif inSolution == "n":
                    print(">>> sortie...")
                    break
                else:
                    print(">>> rentrez une instruction valide!")
            break
        else:
                print(">>> inserez des valeurs numeriques superieur a 1!")
    
#cette methode contient les lignes de commandes de "l'accueil" du programme de génération de labyrinthe
#des instructions sont demandés à l'utilisateur pour demander ce qu'il souhaite faire:
# - générer un labyrinthe puis un chemin entre l'entrée et la sortie
# - fermer le programme
def execution_programme():
    print(">>> bienvenue dans le module de generation de labyrinthe")
    print(">>> initialisation...")
    while True:
        reponse1 = input(">>> voulez vous generer un labyrinthe ? (o:oui/n:non)")
        if reponse1 == "o":
            programme_labyrinthe()
        elif reponse1 == "n":
            reponse2 = input(">>> voulez vous sortir du programme? (o:oui/n:non)")
            if reponse2 == "o":
                print(">>> fermeture du programme...")
                break
            elif reponse2 != "n":
                print(">>> rentrez une instruction valide!")
        else:
            print(">>> rentrez une instruction valide!")









