#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Stephane Wilhelm - Laura Verin
@date  : 11/2018
"""
# -*- coding: utf-8 -*-

import numpy
import random
import matplotlib.pyplot as pyplot

#Cette methode permet de créer notre grille rectangulaire qui constitura le labyrinthe selon une largeur et une hauteur donnée
"""
largeur : valeur de largeur insérée en entrée par l'utilisateur
hauteur : valeur de hauteur insérée en entrée par l'utilisateur
"""
def labyrinthe_ini(largeur, hauteur):
    #la largeur et la hauteur doivent être suppérieur à 1
    if largeur <= 1 or hauteur <= 1:
        return None
    #Permet de considérer une case comme unité de longeur ou largeur
    """
    l : longueur de la matrice
    h : hauteur de la matrice
    """
    l = largeur*2+1
    h = hauteur*2+1
    #Nous générons une matrice
    L = numpy.random.randint(2, size=(l,h))
    #le labyrinthe va être généré sous forme de matrice ou la valeur "0" correspond à une case vide
    #et "1" correspond à un mur
    #Générer un contour dont les valeurs sont égales à 1
    #notre labyrinthe ressemble à ceci:
#    111111111
#    101010101
#    111111111
#    101010101
#    111111111
    for i in range(l):
        for j in range(h):
            if i == 0 or j == 0:
                L[i][j]= 1
            if i == l-1 or j == h-1:
                L[i][j]= 1
            #Génération d'un quadrillage : l'intérieur du labyrinte où toutes les cases sont séparés par des murs "1"
            if i % 2 != 0 and j % 2 != 0:
                L[i][j] = 0
            else :
                L[i][j] = 1
    return L

#nous pour créer l'intérieur de notre labyrinthe à partir du quadrillage, nous utilisons la méthode par fusion aléatoire de chemin
#pour cela nous devons donner un numéro à chaque cases vides 
"""
L : quadrillage du labyrinthe
"""    

def labyrinthe_numerot_cases(L):
    """
    c : conteur pour insérer un numéro unique dans une case du quadrillage
    """
    c = 2 
    for i in range(1, len(L)-1):
        for j in range (1, len(L[0])-1):
            if i % 2 != 0 and j % 2 != 0:
                L[i][j] = c
                c += 1
    return L

#nous insérons les valeurs de chaque case numérotée dans une liste 
def liste_valeurs_numero(L):
    """
    Liste : liste des valeurs numérotées dans le quadrillage
    """

    liste = []
    for i in range (1,len(L)-1) :
        for j in range (1,len(L[0])-1) :
            if i % 2 != 0 and j % 2 == 0 or i % 2 == 0 and j % 2 != 0:
                liste += [[i, j]]
    return liste

#pour la génération de notre chemin, nous devons séléctionner un mur aléatoirement entre deux cases vides et retenir le numéro de celles ci
#nous utilisons la liste précédement créée pour ne pas revenir sur les mêmes valeurs
def recherche_valeur_labyrinthe(L,liste_valeur,alea):
    """
    cle_ref : cle de reference d'une valeur d'un des cotés du mur
    cle_a_remplacer : cle a remplacer de la valeur de l'autre côté du mur
    """
    cle_ref = 0
    cle_a_remplacer = 0    
    
    for i in range (1,len(L)-1):
        for j in range (1,len(L[0])-1):
            if L[i][j] not in liste_valeur:
                liste_valeur.append(L[i][j])    #chaque valeur différente de la matrice est inséré dans la liste si la valeur n'est pas déja présente
                # si les coordonnées correspondent à l'itération de la boucle
            if alea == [i, j] and L[i][j] == 1:
                    # si nous sommes dans un indice d'itération impaire et les cases voisines ont une valeur différente alors, nous changeons le 1 en 0
                if alea[0] % 2 != 0 and L[i][j+1] != L[i][j-1]:
                    L[i][j] = 0
                        # comparaison des valeurs voisines, la clé de référence est la valeur minimale, la clé à remplacer est la valeur maximale
                    if L[i][j-1] < L[i][j+1]:
                        cle_ref = L[i][j-1]
                        cle_a_remplacer = L[i][j+1]
                    else :
                        cle_ref = L[i][j+1]
                        cle_a_remplacer = L[i][j-1]
                       # nous traitons le cas des indices d'itération paires     
                elif alea[0] % 2 == 0 and L[i+1][j] != L[i-1][j]:
                    L[i][j] = 0
                    if L[i-1][j] < L[i+1][j]:
                        cle_ref = L[i-1][j]
                        cle_a_remplacer = L[i+1][j]  
                    else :
                        cle_ref = L[i+1][j]
                        cle_a_remplacer = L[i-1][j]
           
    return cle_ref, cle_a_remplacer

"""
liste_val_num : liste générée à partir de la méthode liste_valeurs_numero
"""

def generation_labyrinthe_interieur(L, liste_val_num):
    
    liste_valeur = [] #Liste des valeurs du labyrinthe
    while len(liste_valeur) != 3: #Tant qu'il y a plus que trois valeur dans la liste...
        liste_valeur = [] # Reinitialiser la liste à 0
        alea = random.choice(liste_val_num)
        #choix de coordonnées aléatoires pour la valeur "1"
        a = recherche_valeur_labyrinthe(L, liste_valeur, alea)
        #les coordonnées tirées aléatoirements présentes dans la liste sont alors supprimées       
        if alea in liste_val_num:
            liste_val_num.remove(alea)
        #les indices de cases sont remplacés par la clé de référence déterminée en amont
        for i in range(1, len(L)-1):
            for j in range(1, len(L[0])-1):
                if L[i][j] == a[1]:
                    L[i][j] = a[0]
                    
    #la valeur finale de la clé de référence est remplacée par 0 pour la visualisation du labyrinthe
    for i in range(1, len(L)-1):
        for j in range(1, len(L[0])-1):
            if L[i][j] != 1:
                L[i][j] = 0
    return L


#l'intérieur du labyrinthe est généré à l'aide des méthodes créées en amont
def generation_labyrinthe(h,l):
    
    laby1 = labyrinthe_ini(l,h)
    laby2 = labyrinthe_numerot_cases(laby1)
    laby3 = generation_labyrinthe_interieur(laby2,liste_valeurs_numero(laby2))
    return laby3

#le labyrinte est affiche sur une interface graphique pyplot
def affichage_labyrinthe(L):
    #taille de la figure et utilisation de la méthode d'affihage binary qui affiche les murs en couleur noir et les cases vides en blanc
    pyplot.figure(figsize=(10,12))
    pyplot.imshow(L, cmap=pyplot.cm.binary, interpolation='nearest')
    #cacher la graduation des abcisses et des ordonnées    
    #point vert qui indique le point de départ
    pyplot.plot(1,1,'bs', color ='green', markersize=8)
    #point bleu qui indique le point d'arrivée
    pyplot.plot(len(L[0])-2,len(L)-2,'bs', color ='red', markersize=8)
    #masque des abcisses et des ordonnées
    pyplot.xticks([]), pyplot.yticks([])
    pyplot.show()
