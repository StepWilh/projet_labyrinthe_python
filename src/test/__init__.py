#!/usr/bin/env python3

import unittest
import sys
sys.path.append("src/test")
from unit_test_labyrinthe_generation import LabyGenerationTestCase
from unit_test_chemin import CheminGenerationTestCase

if __name__ == "__main__":
    unittest.main()

