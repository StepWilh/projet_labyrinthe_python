#!/usr/bin/env python3
import unittest
import sys
sys.path.insert(0, '../main')
sys.path.append("../../src/main")
sys.path.append("src/main")
from chemin_generation import *
#from src.main.laby_exe import *


class CheminGenerationTestCase(unittest.TestCase):
    laby_test = [[1, 1, 1, 1, 1, 1, 1, 1, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 1, 1, 0, 1, 1, 1, 0, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 0, 1, 1, 1, 1, 1, 0, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 0, 0, 1, 0, 1],
                 [1, 1, 1, 1, 1, 1, 1, 1, 1]]

    intersect = [[5, 1]]

    chemin_expect = [[1, 1], [1, 2], [1, 3], [2, 3], [3, 3], [3, 2], [3, 1], [4, 1], [5, 1], [5, 2], [5, 3], [6, 3], [7, 3], [7, 4], [7, 5], [6, 5], [5, 5], [5, 6], [5, 7], [6, 7], [7, 7]]

    #def test_test_intersect(self):

    def test_deplacement_position(self):
        """
                [[1, 1, 1, 1, 1, 1, 1, 1, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 1, 1, 0, 1, 1, 1, 0, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 0, 1, 1, 1, 1, 1, 0, 1],
                 [1,{0},0, 0, 1, 0, 0, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 0, 0, 1, 0, 1],
                 [1, 1, 1, 1, 1, 1, 1, 1, 1]]
        """

        pos = [5, 1]
        intersect = [[5, 1], [5, 7], [3, 7]]
        val_interdit = [[4, 1], [6, 1], [7, 1]]
        #deplacement_position(pos, [N,S,E,W], list_interdit, list_intersect)
        new_pos = deplacement_position(pos, [0, 0, 0, 1], val_interdit, intersect)
        self.assertListEqual(new_pos, [5, 2])

    def test_cul_de_sac(self):
        """
                [[1, 1, 1, 1, 1, 1, 1, 1, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 1, 1, 0, 1, 1, 1, 0, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 0, 1, 1, 1, 1, 1, 0, 1],
                 [1, 0 ,0, 0, 1, 0, 0, 0, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1,{2},1, 0, 0,{3},1, 0, 1],
                 [1, 1, 1, 1, 1, 1, 1, 1, 1]]

                 {2} : cul de sac
                 {3} : pas cul de sac
        """
        #cul_de_sac(pos, voisins_pos, intersect)

        pos = [7, 1]
        voisins_pos = [0, 1, 1, 1]
        intersect = [[5, 1]] # dernières intersections rencontrées en cours de chemin
        self.assertListEqual(cul_de_sac(pos, voisins_pos, intersect), [5, 1])
        self.assertListEqual(cul_de_sac([7, 5], [0, 1, 1, 0], intersect), [7, 5])

    def test_chemin_faux(self):
        """
                [[1, 1, 1, 1, 1, 1, 1, 1, 1],
                 [1, 0, 0, 0, 1, 0, 0, 0, 1],
                 [1, 1, 1, 0, 1, 1, 1,{3}, 1],
                 [1, 0, 0, 0, 1, 0,{3},{2},1],
                 [1, 0, 1, 1, 1, 1, 1,{3}, 1],
                 [1, 0 ,0, 0, 1, 0, 0,{4}, 1],
                 [1, 0, 1, 0, 1, 0, 1, 0, 1],
                 [1, 0, 1, 0, 0, 0, 1, 0, 1],
                 [1, 1, 1, 1, 1, 1, 1, 1, 1]]

                 {2} : position actuelle
                 {3} : zones interdites pour la position car déja explorées
                 {4} : intersection précédente pour la recherche d'autres chemins
        """
        #chemin_faux(L,pos,intersect, interdit)

        laby_test = [[1, 1, 1, 1, 1, 1, 1, 1, 1],
                     [1, 0, 0, 0, 1, 0, 0, 0, 1],
                     [1, 1, 1, 0, 1, 1, 1, 0, 1],
                     [1, 0, 0, 0, 1, 0, 0, 0, 1],
                     [1, 0, 1, 1, 1, 1, 1, 0, 1],
                     [1, 0, 0, 0, 1, 0, 0, 0, 1],
                     [1, 0, 1, 0, 1, 0, 1, 0, 1],
                     [1, 0, 1, 0, 0, 0, 1, 0, 1],
                     [1, 1, 1, 1, 1, 1, 1, 1, 1]]

        pos = [3, 7]
        intersect = [[5, 1], [5, 7], [3, 7]]
        interdit = [[2, 7], [4, 7], [3, 6]]

        self.assertListEqual(chemin_faux(laby_test, pos, intersect, interdit), [5, 7])
        self.assertListEqual(chemin_faux(laby_test, [4, 7], intersect, interdit), [4, 7])

    def test_chemin_final(self):
        #liste à doublons au niveau des intersections
        chemin_entree = [[1, 1], [1, 1], [1, 2], [1, 2], [1, 3]]
        chemin_expect = [[1, 1], [1, 2], [1, 3]]

        self.assertListEqual(chemin_final(chemin_entree), chemin_expect)


    def test_chemin(self):
        laby_test = [[1, 1, 1, 1, 1, 1, 1, 1, 1],
                     [1, 0, 0, 0, 1, 0, 0, 0, 1],
                     [1, 1, 1, 0, 1, 1, 1, 0, 1],
                     [1, 0, 0, 0, 1, 0, 0, 0, 1],
                     [1, 0, 1, 1, 1, 1, 1, 0, 1],
                     [1, 0, 0, 0, 1, 0, 0, 0, 1],
                     [1, 0, 1, 0, 1, 0, 1, 0, 1],
                     [1, 0, 1, 0, 0, 0, 1, 0, 1],
                     [1, 1, 1, 1, 1, 1, 1, 1, 1]]

        chemin_expect = [[1, 1], [1, 2], [1, 3], [2, 3], [3, 3], [3, 2], [3, 1], [4, 1], [5, 1], [5, 2], [5, 3], [6, 3],
                         [7, 3], [7, 4], [7, 5], [6, 5], [5, 5], [5, 6], [5, 7], [6, 7], [7, 7]]

        self.assertListEqual(chemin(laby_test, 4, 4), chemin_expect)

""""
if __name__ == "__main__":
    unittest.main()
"""