#!/usr/bin/env python3
import unittest
import numpy
import sys
#sys.path.insert(0, '../main')
sys.path.append("../../src/main")
sys.path.append("src/main")

from labyrinthe_generation import *


class LabyGenerationTestCase(unittest.TestCase):

    def test_labyrinthe_ini(self):

        laby_expect = [[1, 1, 1, 1, 1], [1, 0, 1, 0, 1], [1, 1, 1, 1, 1], [1, 0, 1, 0, 1], [1, 1, 1, 1, 1], [1, 0, 1, 0, 1], [1, 1, 1, 1, 1]]
        laby_result = numpy.array(labyrinthe_ini(3, 2)).tolist()

        self.assertCountEqual(laby_result, laby_expect)
        self.assertListEqual(laby_result, laby_expect)
        self.assertIsNone(labyrinthe_ini(-1, -2))
        with self.assertRaises(TypeError):
            labyrinthe_ini("n", "t")

    def test_labyrinthe_numerot_cases(self):
        laby_in = [[1, 1, 1, 1, 1], [1, 0, 1, 0, 1], [1, 1, 1, 1, 1], [1, 0, 1, 0, 1], [1, 1, 1, 1, 1],
                   [1, 0, 1, 0, 1], [1, 1, 1, 1, 1]]

        laby_expect = [[1, 1, 1, 1, 1], [1, 2, 1, 3, 1], [1, 1, 1, 1, 1], [1, 4, 1, 5, 1], [1, 1, 1, 1, 1], [1, 6, 1, 7, 1], [1, 1, 1, 1, 1]]
        self.assertListEqual(labyrinthe_numerot_cases(laby_in),laby_expect)

    def test_liste_valeurs_numero(self):
        laby_in = [[1, 1, 1, 1, 1], [1, 2, 1, 3, 1], [1, 1, 1, 1, 1], [1, 4, 1, 5, 1], [1, 1, 1, 1, 1], [1, 6, 1, 7, 1], [1, 1, 1, 1, 1]]
        laby_expect = [[1, 2], [2, 1], [2, 3], [3, 2], [4, 1], [4, 3], [5, 2]]

        self.assertListEqual(liste_valeurs_numero(laby_in),laby_expect)

    def test_recherche_valeur_labyrinthe(self):
        laby_in = [[1, 1, 1, 1, 1], [1, 2, 1, 3, 1], [1, 1, 1, 1, 1], [1, 4, 1, 5, 1], [1, 1, 1, 1, 1], [1, 6, 1, 7, 1], [1, 1, 1, 1, 1]]
        laby_coord_list = [[1, 2], [2, 1], [2, 3], [3, 2], [4, 1], [4, 3], [5, 2]]
        alea = [1, 2]

        self.assertEqual(recherche_valeur_labyrinthe(laby_in, laby_coord_list, alea), (2, 3))

    def test_generation_labyrinthe_interieur(self):
        laby_in = [[1, 1, 1, 1, 1], [1, 2, 1, 3, 1], [1, 1, 1, 1, 1], [1, 4, 1, 5, 1], [1, 1, 1, 1, 1], [1, 6, 1, 7, 1], [1, 1, 1, 1, 1]]
        laby_coord_list = [[1, 2], [2, 1], [2, 3], [3, 2], [4, 1], [4, 3], [5, 2]]

        laby_generated = generation_labyrinthe_interieur(laby_in, laby_coord_list)

        # valeur des numéro de case du labyrinte restants
        laby_list_val_case = [];

        for i in laby_generated:
            for j in i:
                if j not in laby_list_val_case:
                    laby_list_val_case.append(j)

        self.assertListEqual(laby_list_val_case, [1, 0])

    def test_generation_labyrinthe(self):
        laby_result = generation_labyrinthe(3, 2)

        laby_list_val_case = [];

        for i in laby_result:
            for j in i:
                if j not in laby_list_val_case:
                    laby_list_val_case.append(j)

        self.assertListEqual(laby_list_val_case, [1, 0])


#if __name__ == '__main__':
#    unittest.main()


